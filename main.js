// node backend that returns whatever body you send it with the URL path added to it

const express = require('express');
const app = express();
const fs = require('fs');

// body can be in any format, so we need to parse it
app.use(express.urlencoded({ extended: true }));
app.use(express.text());
app.use(express.json());
app.use(express.raw());


app.get('/', (req, res) => {
    let body = {};
    body.info = 'This was a GET request';
    body.method = 'DELETE';
    body.path = req.path;
    body.timestamp = Date.now();
    res.send('This was a GET request');
    // add row to log file
    fs.appendFile('log.txt', body, (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });
}
);

app.post('*', (req, res) => {
    let body = {}

    // add body to body
    body.body = JSON.stringify(req.body);
    // add method and path to body
    body.method = 'POST';
    body.path = req.path;
    body.timestamp = Date.now();

    res.send(body);
    // add row as a string to log file as a new line
    fs.appendFile('log.txt', JSON.stringify(body) + '\n', (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });



});

app.put('*', (req, res) => {
    let body = {}

    // add body to body
    body.body = JSON.stringify(req.body);
    // add method and path to body
    body.method = 'PUT';
    body.path = req.path;
    body.timestamp = Date.now();

    res.send(body);
    // add row as a string to log file as a new line
    fs.appendFile('log.txt', JSON.stringify(body) + '\n', (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });
}
);

app.patch('*', (req, res) => {
    let body = {}

    // add body to body
    body.body = JSON.stringify(req.body);
    // add method and path to body
    body.method = 'PATCH';
    body.path = req.path;
    body.timestamp = Date.now();

    res.send(body);

    // add row as a string to log file as a new line
    fs.appendFile('log.txt', JSON.stringify(body) + '\n', (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });
}
);

app.delete('*', (req, res) => {
    let body = {}

    // add body to body
    body.body = JSON.stringify(req.body);
    // add method and path to body
    body.method = 'DELETE';
    body.path = req.path;
    body.timestamp = Date.now();

    res.send(body);
    // add row as a string to log file as a new line
    fs.appendFile('log.txt', JSON.stringify(body) + '\n', (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });
}
);

app.listen(4200, () => console.log('Listening on port 4200!'));